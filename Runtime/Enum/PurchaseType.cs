namespace com.foundation.iap.core
{
    public enum PurchaseType
    {
        Consumable = 0,
        NonConsumable = 1,
        Subscription = 2
    }
}