namespace com.foundation.iap.core
{
    public enum PurchaseState
    {
        Unknown = -1,
        Purchased = 0,
        Canceled = 1,
        Pending = 2,
    }
}
