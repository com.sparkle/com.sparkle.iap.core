namespace com.foundation.iap.core
{
    public enum InitializationStatus
    {
        Ready = 0,
        NotReady = 1
    }
}